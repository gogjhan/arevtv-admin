export default class {
    constructor () {
        this.slug = {
            en: '',
            ru: '',
            hy: ''
        }

        this.metaDescription = {
            en: '',
            ru: '',
            hy: ''
        }

        this.metaKeywords = {
            en: '',
            ru: '',
            hy: ''
        }

        this.headTitle = {
            en: '',
            ru: '',
            hy: ''
        }

        this.title = {
            en: '',
            ru: '',
            hy: ''
        }

        this.summary = {
            en: '',
            ru: '',
            hy: ''
        }

        this.cover = null
        // this.quality = {
        //     760: '',
        //     480: '',
        //     360: ''
        // }
        this.order = 0
        this.type = 'trailer'
        this.premium = false
    }
}
