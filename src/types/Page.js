export default class {
    constructor () {
        this.title = {
            en: '',
            ru: '',
            hy: ''
        }

        this.content = {
            en: '',
            ru: '',
            hy: ''
        }

        this.image = ''
        this.slug = ''
    }
}
