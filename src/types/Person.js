export default class {
    constructor () {
        this.fullName = {
            en: '',
            ru: '',
            hy: ''
        }

        this.biography = {
            en: '',
            ru: '',
            hy: ''
        }

        this.image = ''
        this.type = ''
    }
}
