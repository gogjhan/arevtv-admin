export default class {
    constructor () {
        this.slug = {
            en: '',
            ru: '',
            hy: ''
        }

        this.metaDescription = {
            en: '',
            ru: '',
            hy: ''
        }

        this.metaKeywords = {
            en: '',
            ru: '',
            hy: ''
        }

        this.headTitle = {
            en: '',
            ru: '',
            hy: ''
        }

        this.title = {
            en: '',
            ru: '',
            hy: ''
        }

        this.summary = {
            en: '',
            ru: '',
            hy: ''
        }

        this.poster = null
        this.cover = null
        this.director = ''
        this.actors = []
        this.url = ''
        this.popular = false
        this.genres = []
        this.duration = 0
        this.published = new Date()
        this.type = ''
        this.seasons = []
        this.trailers = []
        this.episodes = []
        this.premium = true
        this.recommendations = []
    }
}
