export default class {
    constructor () {
        this.name = ''
        this.email = ''
        this.password = ''
        this.passwordConfirmation = ''
        this.newPassword = ''
        this.admin = false
    }
}
