export default class {
    constructor () {
        this.liveVideoURI = ''
        this.liveVideoTitle = {
            en: '',
            ru: '',
            hy: ''
        }

        this.liveVideoDescription = {
            en: '',
            ru: '',
            hy: ''
        }
    }
}
