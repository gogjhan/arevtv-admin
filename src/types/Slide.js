export default class {
    constructor () {
        this.title = { en: '', ru: '', hy: '' }
        this.content = { en: '', ru: '', hy: '' }
        this.action = ''
        this.page = 'home'
        this.cover = ''
        this.order = 0
    }
}
