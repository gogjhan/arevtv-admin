import Vue from 'vue'
import Vuex from 'vuex'
import { Message, Notification } from 'element-ui'
import http from './http'
import router from './router'

import projects from './store/projects'
import people from './store/people'
import slides from './store/slides'
import users from './store/users'
import ads from './store/ads'
import pages from './store/pages'
import videos from './store/videos'
import settings from './store/settings'

Vue.use(Vuex)

const state = {
    loading: false,
    baseURL: `${window.location.protocol}//${window.location.hostname}:4004/`,
    user: {},
    login: {
        username: '',
        password: ''
    }
}

const actions = {
    handleError ({ state, commit, dispatch }, e) {
        console.log(e)
        console.log(e.response)
        commit('LOADED')

        if (e.response) {
            let error = e.response.data
            if (error.name === 'ValidationError') {
                Object.values(error.errors).forEach(error => {
                    setTimeout(() => {
                        Notification.error({ title: error.message, message: 'Validation error' })
                    }, 10)
                })
            } else if (error.message) {
                Message({
                    message: error.message,
                    type: 'error'
                })
            } else if (error.errmsg) {
                Notification.error({ title: error.errmsg, message: 'Validation error' })
            }
        }
    },
    async login ({ state, commit, dispatch }) {
        try {
            await http.post('auth/local', state.login)
            router.push({ name: 'projects', params: { type: 'movies' } })
        } catch (e) {
            if (e.response.status === 401) {
                Message.error({
                    message: 'Invalid password or email!'
                })
            }

            dispatch('handleError', e)
        }
    },
    async logout ({ state, commit, dispatch }) {
        try {
            await http.get('auth/logout')
            commit('SET', { key: 'user', value: {} })
            router.push({ name: 'login' })
        } catch (e) {
            dispatch('handleError', e)
        }
    },
    async getUser ({ state, commit, dispatch }) {
        try {
            let response = await http.get('api/user')
            commit('SET', { key: 'user', value: response.data.user })
        } catch (e) {
            if (e.response.status === 401) {
                router.push({ name: 'login' })
                return
            }

            dispatch('handleError', e)
        }
    }
}

const mutations = {
    SET: (state, { key, value }) => { state[key] = value },
    LOADING: (state) => { state.loading = true },
    LOADED: (state) => { state.loading = false },
    LOGIN: (state, token) => { localStorage.setItem('token', token); window.location = '/' }
}

const getters = {}

export default new Vuex.Store({
    state,
    actions,
    mutations,
    getters,
    modules: {
        projects,
        people,
        slides,
        users,
        ads,
        pages,
        videos,
        settings
    }
})
