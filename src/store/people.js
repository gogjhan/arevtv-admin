import http from '../http'
import Person from '../types/Person'
import { MessageBox } from 'element-ui'

const state = {
    all: [],
    person: new Person(),
    dialogs: {
        person: false
    }
}

const actions = {
    async all ({ state, commit, dispatch }, params) {
        try {
            let response = await http.get('api/people', { params })
            commit('SET_PEOPLE', response.data.people)
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async submit ({ state, commit, dispatch }, update) {
        try {
            let data = new FormData()
            Object.entries(state.person).forEach(([key, value]) => {
                if (value && typeof value === 'object' && 'en' in value) {
                    data.append(key, JSON.stringify(value))
                } else {
                    data.append(key, value)
                }
            })

            let response
            if (update)
                response = await http.put(`api/admin/people/${state.person._id}`, data)
            else
                response = await http.post(`api/admin/people`, data)

            if (response.data.errors) { // handle mongoose errors
                dispatch('handleError', response.data, { root: true })
                return
            }

            commit('HIDE_PERSON_DIALOG')
            dispatch('all')
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async remove ({ state, commit, dispatch }, personId) {
        try {
            await MessageBox.confirm('This will permanently delete information. Continue ?', 'Warning')
            await http.delete(`api/admin/people/${personId}`)
            dispatch('all')
        } catch (e) { dispatch('handleError', e, { root: true }) }
    }
}

const mutations = {
    SET_PEOPLE: (state, people) => { state.all = people },
    SET_IMAGE: (state, image) => { state.person.image = image },
    SHOW_PERSON_DIALOG: (state) => {
        state.dialogs.person = true
        state.person = new Person()
    },
    EDIT: (state, person) => {
        state.dialogs.person = true
        state.person = {...person}
    },
    HIDE_PERSON_DIALOG: (state) => { state.dialogs.person = false }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
