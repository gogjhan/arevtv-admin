import http from '../http'
import User from '../types/User'
import { MessageBox } from 'element-ui'

const state = {
    all: [],
    user: new User(),
    dialogs: {
        user: false
    }
}

const actions = {
    async all ({ state, commit, dispatch }, params) {
        try {
            let response = await http.get('api/admin/users')
            commit('SET', { key: 'all', value: response.data.users })
        } catch (e) {
            dispatch('handleError', e, { root: true })
        }
    },
    async submit ({ state, commit, dispatch }, update) {
        try {
            if (update) {
                await http.put(`api/admin/users/${state.user._id}`, state.user)
            } else {
                await http.post(`auth/register`, state.user)
            }

            commit('HIDE_USER_DIALOG')
            dispatch('all')
        } catch (e) {
            dispatch('handleError', e, { root: true })
        }
    },
    async remove ({ state, commit, dispatch }, userId) {
        try {
            await MessageBox.confirm('This will permanently delete information. Continue ?', 'Warning')
            await http.delete(`api/admin/users/${userId}`)
            dispatch('all')
        } catch (e) {
            dispatch('handleError', e, { root: true })
        }
    }
}

const mutations = {
    SET: (state, { key, value }) => { state[key] = value },
    SET_IMAGE: (state, image) => { state.user.image = image },
    SHOW_USER_DIALOG: (state) => {
        state.dialogs.user = true
        state.user = new User()
    },
    HIDE_USER_DIALOG: (state) => { state.dialogs.user = false },
    EDIT: (state, user) => {
        state.dialogs.user = true
        state.user = {...user}
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
