import http from '../http'
import Slide from '../types/Slide'
import { MessageBox } from 'element-ui'

const state = {
    all: [],
    slide: new Slide(),
    dialogs: {
        slide: false
    }
}

const actions = {
    async all ({ state, commit, dispatch }, { page }) {
        try {
            let response = await http.get('api/slides', { params: { search: { page } } })
            commit('SET_SLIDES', response.data.slides)
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async submit ({ state, commit, dispatch }, update) {
        try {
            let data = new FormData()
            Object.entries(state.slide).forEach(([key, value]) => {
                if (value && typeof value === 'object' && 'en' in value) {
                    data.append(key, JSON.stringify(value))
                } else {
                    data.append(key, value)
                }
            })

            let response
            if (update)
                response = await http.put(`api/admin/slides/${state.slide._id}`, data)
            else
                response = await http.post('api/admin/slides', data)

            if (response.data.errors) { // mongoose errors
                dispatch('handleError', response.data, { root: true })
                return
            }

            dispatch('all', { page: state.slide.page })
            commit('HIDE_SLIDE_DIALOG')
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async reorder ({ state, commit, dispatch }, orderedSlides) {
        try {
            commit('SET_SLIDES', orderedSlides)
            let updates = []
            orderedSlides.forEach((slide, index) => {
                updates.push(http.put(`api/admin/slides/${slide._id}`, { order: index + 1 }))
            })

            await Promise.all(updates)
            dispatch('all', { page: orderedSlides[0].page })
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async remove ({ state, commit, dispatch }, slideId) {
        try {
            await MessageBox.confirm('This will permanently delete information. Continue ?', 'Warning')
            await http.delete(`api/admin/slides/${slideId}`)
            dispatch('all')
        } catch (e) { dispatch('handleError', e, { root: true }) }
    }
}

const mutations = {
    SET: (state, { key, value }) => { state[key] = value },
    SET_SLIDES: (state, slides) => { state.all = slides },
    SET_COVER: (state, cover) => { state.slide.cover = cover },
    SHOW_SLIDE_DIALOG: (state) => {
        state.dialogs.slide = true
        state.slide = new Slide()
    },
    HIDE_SLIDE_DIALOG: (state) => {
        state.dialogs.slide = false
        state.slide = new Slide()
    },
    EDIT: (state, index) => {
        state.dialogs.slide = true
        state.slide = {...state.all[index]}
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
