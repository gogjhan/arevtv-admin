import http from '../http'
import Ad from '../types/Ad'

const state = {
    all: [],
    ad: new Ad(),
    dialogs: {
        ad: false
    }
}

const actions = {
    async all ({ state, commit, dispatch }) {
        try {
            let response = await http.get('api/ads')
            commit('SET_ADS', response.data.ads)
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async submit ({ state, commit, dispatch }, update) {
        try {
            let data = new FormData()
            Object.entries(state.ad).forEach(([key, value]) => {
                if (value && typeof value === 'object' && 'en' in value) {
                    data.append(key, JSON.stringify(value))
                } else {
                    data.append(key, value)
                }
            })

            if (update)
                await http.put(`api/admin/ads/${state.ad._id}`, data)
            else
                await http.post(`api/admin/ads`, data)

            commit('HIDE_AD_DIALOG')
            dispatch('all')
        } catch (e) { dispatch('handleError', e, { root: true }) }
    }
}

const mutations = {
    SET_ADS: (state, ads) => { state.all = ads },
    SET_IMAGE: (state, image) => { state.ad.image = image },
    SHOW_AD_DIALOG: (state) => {
        state.dialogs.ad = true
        state.ad = new Ad()
    },
    EDIT: (state, ad) => {
        state.dialogs.ad = true
        state.ad = {...ad}
    },
    HIDE_AD_DIALOG: (state) => { state.dialogs.ad = false }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
