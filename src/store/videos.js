import http from '../http'
import Video from '../types/Video'
import { MessageBox } from 'element-ui'

const state = {
    dialog: false,
    editDialog: false,
    all: [],
    trailers: [],
    episodes: [],
    video: new Video(),
    type: 'trailer'
}

const actions = {
    async get ({ state, commit, dispatch }, options) {
        try {
            if (options && options.type) {
                commit('SET', { key: 'type', value: options.type })
            }

            let response = await http.get('api/videos', { params: { type: state.type } })

            if (options && options.type) {
                commit('SET', { key: `${options.type}s`, value: response.data.videos })
            } else {
                commit('SET', { key: 'all', value: response.data.videos })
            }
        } catch (e) {
            dispatch('handleError', e, { root: true })
        }
    },
    async submit ({ state, commit, dispatch }, { update }) {
        try {
            let data = new FormData()
            Object.entries(state.video).forEach(([key, value]) => {
                if (value && typeof value === 'object' && ('en' in value || 720 in value)) {
                    data.append(key, JSON.stringify(value))
                } else {
                    data.append(key, value)
                }
            })

            if (update)
                await http.put(`api/admin/videos/${state.video._id}`, data)
            else
                await http.post(`api/admin/videos`, data)

            dispatch('get')
            commit('HIDE')
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async remove ({ state, commit, dispatch }, video) {
        try {
            await MessageBox.confirm('This will permanently delete information. Continue ?', 'Warning')
            await http.delete(`api/admin/videos/${video._id}`)
            dispatch('get')
            commit('HIDE')
        } catch (e) {
            dispatch('handleError', e, { root: true })
        }
    }
}

const mutations = {
    SET: (state, { key, value }) => { state[key] = value },
    SET_VIDEO: (state, { key, value }) => { state.video[key] = value },
    ADD: (state) => {
        state.editDialog = true
        state.video = new Video()
    },
    EDIT: (state, video) => {
        state.editDialog = true
        state.video = {...video}
    },
    HIDE: (state) => { state.editDialog = false }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
