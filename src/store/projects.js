import { MessageBox } from 'element-ui'
import http from '../http'
import Project from '../types/Project'

const state = {
    all: [],
    project: new Project(),
    dialogs: {
        project: {
            visibility: false,
            type: 'movie'
        }
    }
}

const actions = {
    async all ({ state, commit, dispatch }, params) {
        try {
            let response = await http.get('api/projects', { params })
            commit('SET_PROJECTS', response.data.projects.all)
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async submit ({ state, commit, dispatch }, update) {
        try {
            let data = new FormData()
            Object.entries(state.project).forEach(([key, value]) => {
                if ((typeof value === 'object' && value && 'en' in value) || Array.isArray(value)) {
                    data.append(key, JSON.stringify(value))
                } else {
                    data.append(key, value)
                }
            })

            if (update) {
                await http.put(`api/admin/projects/${state.project._id}`, data)
            } else {
                await http.post('api/admin/projects', data)
            }

            commit('HIDE_PROJECT_DIALOG')
            dispatch('all', { filters: { type: state.project.type } })
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async remove ({ state, commit, dispatch }, project) {
        try {
            await MessageBox.confirm('This will permanently delete information. Continue ?', 'Warning')
            await http.delete(`api/admin/projects/${project._id}`)
            dispatch('all', { filters: { type: project.type } })
        } catch (e) { dispatch('handleError', e, { root: true }) }
    }
}

const mutations = {
    SET_PROJECTS: (state, projects) => { state.all = projects },
    SHOW_PROJECT_DIALOG: (state, type) => {
        state.dialogs.project = { visibility: true, type }
        state.project = new Project()
        state.project.type = type
    },
    EDIT: (state, project) => {
        state.dialogs.project = { visibility: true, type: project.type }
        state.project = new Project()
        state.project = {...project}
    },
    HIDE_PROJECT_DIALOG: (state) => { state.dialogs.project.visibility = false },
    SET_POSTER: (state, poster) => { state.project.poster = poster },
    SET_COVER: (state, cover) => { state.project.cover = cover },
    SET_BANNER: (state, banner) => { state.project.banner = banner },
    ADD_GENRE: (state, genre) => { if (genre.length) state.project.genres.push(genre) },
    REMOVE_GENRE: (state, index) => { state.project.genres.splice(index, 1) },
    ADD_TRAILER: (state, trailer) => { if (trailer.length) state.project.trailers.push(trailer) },
    REMOVE_TRAILER: (state, index) => { state.project.trailers.splice(index, 1) },
    ADD_EPISODE: (state, episode) => { if (episode.length) state.project.episodes.push(episode) },
    REMOVE_EPISODE: (state, index) => { state.project.episodes.splice(index, 1) },
    NEW_SEASON: (state) => { state.project.seasons.push({ reference: null, name: '' }) },
    REMOVE_SEASON: (state, index) => { state.project.seasons.splice(index, 1) }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
