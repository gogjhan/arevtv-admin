import http from '../http'
import Settings from '../types/Settings'

import {
    Message
} from 'element-ui'

const state = {
    settings: new Settings()
}

const actions = {
    async get ({ state, commit, dispatch }) {
        try {
            let response = await http.get('api/settings')

            commit('SET', { key: 'settings', value: response.data.settings })
        } catch (e) {
            dispatch('handleError', e, { root: true })
        }
    },
    async submit ({ state, commit, dispatch }) {
        try {
            let data = new FormData()
            Object.entries(state.settings).forEach(([key, value]) => {
                if (value && typeof value === 'object' && ('en' in value || 720 in value)) {
                    data.append(key, JSON.stringify(value))
                } else {
                    data.append(key, value)
                }
            })
            await http.put(`api/admin/settings`, data)
            dispatch('get')
            Message.success({ message: 'Successfully updated!' })
        } catch (e) { dispatch('handleError', e, { root: true }) }
    }
}

const mutations = {
    SET: (state, { key, value }) => { state[key] = value }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
