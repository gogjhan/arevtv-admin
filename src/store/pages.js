import http from '../http'
import Page from '../types/Page'
import { MessageBox } from 'element-ui'

const state = {
    all: [],
    page: new Page(),
    dialogs: {
        page: false
    }
}

const actions = {
    async all ({ state, commit, dispatch }) {
        try {
            let response = await http.get('api/pages')
            commit('SET_PAGES', response.data.pages)
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async submit ({ state, commit, dispatch }, update) {
        try {
            let data = new FormData()
            Object.entries(state.page).forEach(([key, value]) => {
                if (value && typeof value === 'object' && 'en' in value) {
                    data.append(key, JSON.stringify(value))
                } else {
                    data.append(key, value)
                }
            })

            if (update)
                await http.put(`api/admin/pages/${state.page._id}`, data)
            else
                await http.post(`api/admin/pages`, data)

            commit('HIDE_PAGE_DIALOG')
            dispatch('all')
        } catch (e) { dispatch('handleError', e, { root: true }) }
    },
    async remove ({ state, commit, dispatch }, pageId) {
        try {
            await MessageBox.confirm('This will permanently delete information. Continue ?', 'Warning')
            await http.delete(`api/admin/pages/${pageId}`)
            dispatch('all')
        } catch (e) { dispatch('handleError', e, { root: true }) }
    }
}

const mutations = {
    SET_PAGES: (state, pages) => { state.all = pages },
    SET_IMAGE: (state, image) => { state.page.image = image },
    SHOW_PAGE_DIALOG: (state) => {
        state.dialogs.page = true
        state.page = new Page()
    },
    EDIT: (state, page) => {
        state.dialogs.page = true
        state.page = {...page}
    },
    HIDE_PAGE_DIALOG: (state) => { state.dialogs.page = false }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
