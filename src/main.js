// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

/**
 * ELEMENT UI
 */
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css'

/**
 * FONT AWESOME
 */
import fontawesome from '@fortawesome/fontawesome'
import brands from '@fortawesome/fontawesome-free-brands'
import solid from '@fortawesome/fontawesome-free-solid'
import regular from '@fortawesome/fontawesome-free-regular'
import FontAwesomeIcon from '@fortawesome/vue-fontawesome'

import tinymce from 'vue-tinymce-editor'
import 'tinymce/skins/lightgray/skin.min.css'

import App from './App'
import router from './router'
import store from './store'

Vue.use(ElementUI, { locale })
Vue.component('icon', FontAwesomeIcon)
Vue.component('tinymce', tinymce)

fontawesome.library.add(brands, solid, regular)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
