import Vue from 'vue'
import Router from 'vue-router'

import Dashboard from '@/components/Dashboard'
import Login from '@/components/Login'

import Projects from '@/components/Projects'
import Slides from '@/components/Slides'
import People from '@/components/People'
import Users from '@/components/Users'
import Ads from '@/components/Ads'
import Pages from '@/components/Pages'
import Videos from '@/components/Videos'
import Settings from '@/components/Settings'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      meta: { requiresAuth: true },
      component: Dashboard,
      children: [
        { path: 'projects/:type', name: 'projects', component: Projects },
        { path: '/slides', name: 'slides', component: Slides },
        { path: '/people', name: 'people', component: People },
        { path: '/users', name: 'users', component: Users },
        { path: '/ads', name: 'ads', component: Ads },
        { path: '/pages', name: 'pages', component: Pages },
        { path: '/videos', name: 'videos', component: Videos },
        { path: '/settings', name: 'settings', component: Settings }
      ]
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    }
  ]
})

export default router
